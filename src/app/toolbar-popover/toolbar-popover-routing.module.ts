import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ToolbarPopoverPage } from './toolbar-popover.page';

const routes: Routes = [
  {
    path: '',
    component: ToolbarPopoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ToolbarPopoverPageRoutingModule {}
