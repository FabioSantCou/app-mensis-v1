import { AlertController, PopoverController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';


@Component({
  selector: 'app-toolbar-popover',
  templateUrl: './toolbar-popover.page.html',
  styleUrls: ['./toolbar-popover.page.scss'],
  providers:[Storage]
})
export class ToolbarPopoverPage implements OnInit {

  constructor(
    private storage:Storage,
    public popoverController: PopoverController, 
    private alertController:AlertController) { }

  async ngOnInit() {
    await this.storage.create();
  }

  async presentAlertConfirm() {
    this.popoverController.dismiss()
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Apagar credenciais !',
      message: 'Deseja <strong>realmente apagar</strong> suas credenciais de conexão ?',
      buttons: [
        {
          text: 'NÃO',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return;
          }
        }, {
          text: 'SIM',
          handler: () => {
            this.storage.set("MAC","")
            this.storage.set("cadastrado","false")
            setTimeout( () => { 
              location.reload()
            }, 1000 );
          }
        }
      ]
    });

    await alert.present();
  }

}
