import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ToolbarPopoverPageRoutingModule } from './toolbar-popover-routing.module';

import { ToolbarPopoverPage } from './toolbar-popover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ToolbarPopoverPageRoutingModule
  ],
  declarations: [ToolbarPopoverPage]
})
export class ToolbarPopoverPageModule {}
