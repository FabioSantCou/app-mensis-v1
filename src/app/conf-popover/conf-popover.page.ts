import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { LoadingService } from './../Services/loading.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PopoverController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';




@Component({
  selector: 'app-conf-popover',
  templateUrl: './conf-popover.page.html',
  styleUrls: ['./conf-popover.page.scss'],
  providers:[FormBuilder,LoadingService,BluetoothSerial,Storage]
})
export class ConfPopoverPage implements OnInit {

  public userForm: FormGroup = new FormGroup({
    'id': new FormControl(null, [Validators.required, Validators.maxLength(6)]),
    'cpf': new FormControl(null,[Validators.required, Validators.minLength(11)]),
    'senha': new FormControl(null, [Validators.required, Validators.minLength(3),Validators.maxLength(11)]),
    'mac': new FormControl(null, [Validators.required, Validators.minLength(17)]),
  })


  private userDefault = {
    id:"XRd9kD",
    cpf:"06535718603",
    senha:"santcou90",
    mac:"30:AE:A4:0B:06:D6"
  }
  // private idDefault="XRd9kD"
  // private MACDefault="30:AE:A4:0B:06:D6"

  constructor(
    public popoverController: PopoverController,
    private bluetoothSerial: BluetoothSerial,
    private loadingService:LoadingService,
    private alertController:AlertController,
    private storage: Storage,
    public formBuilder: FormBuilder) { 

      //   this.userForm = formBuilder.group({
      //     id: ['', Validators.required,Validators.maxLength(6)],
      //     cpf: ['', Validators.required,Validators.maxLength(11)],
      //     senha: ['', Validators.required,Validators.maxLength(11)],
      //     mac: ['', Validators.required]
      // })
  }
  
  async ngOnInit() {
    await this.storage.create();
    this.userForm.setValue({
      //id:this.userDefault.id,
      // cpf:this.userDefault.cpf,
      // senha:this.userDefault.senha,
      // mac:this.userDefault.mac
    })
  }

  public awaitConfirm()
  {
    setTimeout( () => {
      this.bluetoothSerial.read()
      .then((data:string)=>{
        //let dataNomalized = data.toString().substring(1,data.length()-1)
        //let dataNomalized = data.toString().replace('0',"X")
        //alert(data.length)
        let message=''
        for(let i=0; i<data.length; i++)
        {
          if(this.isNumber(data[i]))
          {
            message+=data[i]
          }
        }

        if(message!="OK"){
          //alert(data)
          //this.awaitConfirm()
          this.presentAlertCommand()
          this.loadingService.dismiss()
        }
        else{
          //alert("BATEU"+message)
          this.loadingService.dismiss()
          this.loadingService.presentToast("Credenciais Enviadas com Sucesso !!")
          this.storage.set("cadastrado","true")
          this.storage.set("MAC",(this.userForm.value.mac).toString())
          this.storage.set("SENHA",(this.userForm.value.senha).toString())
          this.storage.set("CPF",(this.userForm.value.cpf).toString())
          this.storage.set("ID",(this.userForm.value.id).toString())
          setTimeout( () => { location.reload(); }, 1000 );
        }
      })
    }, 1000 );
  }

  public sendCredentials()
  { 

    if(this.userForm.value.mac == "" || this.userForm.value.cpf == "" || this.userForm.value.senha == "" || this.userForm.value.id == "")
    {
      this.loadingService.presentToast("<strong>Todos</strong> os campos são de preenchimento obigatório !!")
    }else if(this.userForm.value.mac == null || this.userForm.value.cpf == null || this.userForm.value.senha == null || this.userForm.value.id == null)
    {
      this.loadingService.presentToast("<strong>Todos</strong> os campos são de preenchimento obigatório !!")
    }
    else{

      //console.log((this.userForm.value.mac).toUpperCase())

      this.loadingService.present("Tentando conexão, aguarde !!")
      this.popoverController.dismiss()
      this.popoverController.dismiss()


      this.bluetoothSerial.connect((this.userForm.value.mac).toString())
      .subscribe(data=>{
        this.loadingService.dismiss();
        this.loadingService.present('Enviando Credenciais')
        this.bluetoothSerial.isConnected().then(success => {
          this.bluetoothSerial.write('SendCred').then(success => {
            let jsonCredentials = {
              id:this.userForm.value.id,
              senha:this.userForm.value.senha,
              cpf:this.userForm.value.cpf
            }
            setTimeout( () => { this.bluetoothSerial.write("JSON"+JSON.stringify(jsonCredentials)+"JSON").then(success => {
            //   setTimeout( () => { this.bluetoothSerial.write(this.userForm.value.senha).then(success => {
            //   }, error => {
            //     this.loadingService.presentToast("Erro ao transmitir Credênciais " + error)
            //     setTimeout( () => { location.reload(); }, 3000 );
            //   });}, 3000 );
            // }, error => {
            //   this.loadingService.presentToast("Erro ao transmitir Credênciais " + error)
            //   setTimeout( () => { location.reload(); }, 3000 );
            
            this.awaitConfirm()
            // this.loadingService.dismiss()
            // this.loadingService.presentToast("Credênciais Enviadas com Sucesso!"+jsonCredentials.cpf)
            // this.storage.set("cadastrado","true")
            // this.storage.set("MAC",(this.userForm.value.mac).toString())
            // this.storage.set("SENHA",(this.userForm.value.senha).toString())
            // this.storage.set("CPF",(this.userForm.value.cpf).toString())
            // this.storage.set("ID",(this.userForm.value.id).toString())
            //setTimeout( () => { location.reload(); }, 2000 );
            });}, 2000 );
          }, error => {
            this.loadingService.presentToast("Erro ao transmitir Credênciais " + error)
            setTimeout( () => { location.reload(); }, 3000 );
          });
        }, error => {
          this.loadingService.presentToast("Dispositivo Desconectado " + error)
          setTimeout( () => { location.reload(); }, 3000 );
        });
      },error=>{
        this.loadingService.dismiss();
        this.loadingService.presentToast("Erro na conexão " + error)
        this.loadingService.dismiss()
        setTimeout( () => { location.reload(); }, 3000 );
      })
      
    }
  

    
  }

  async presentAlertCommand() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Falha no envio !',
      message: 'Ocorreu uma <strong>falha no envio das credênciais</strong> de acesso! \n Confira se os campos <strong>ID</strong> e '+
      '<strong>MAC</strong> estão corretos e tente realizar o cadastro novamente.',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            location.reload();
          }
        }
      ]
    });

    await alert.present();
  }

  public isNumber(data:any) : boolean
  { 
    if(data=='0' || data=='1' || data=='2' || data=='3' || data=='4' || data=='5' || data=='6' || data=='7' || data=='8' || data=='9')
    {
      return false
    }else{
      return true
    }
  }
}
