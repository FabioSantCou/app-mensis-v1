import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfPopoverPageRoutingModule } from './conf-popover-routing.module';

import { ConfPopoverPage } from './conf-popover.page';

import {NgxMaskIonicModule} from 'ngx-mask-ionic'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ConfPopoverPageRoutingModule,
    NgxMaskIonicModule
  ],
  declarations: [ConfPopoverPage]
})
export class ConfPopoverPageModule {}
