import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfPopoverPage } from './conf-popover.page';

const routes: Routes = [
  {
    path: '',
    component: ConfPopoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfPopoverPageRoutingModule {}
