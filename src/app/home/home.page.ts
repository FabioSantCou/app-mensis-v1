import { ConfPopoverPage } from './../conf-popover/conf-popover.page';
import { Component, NgZone } from '@angular/core';
import { LoadingService } from '../Services/loading.service';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import { PopoverController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { AlertController } from '@ionic/angular';
import { ToolbarPopoverPage } from '../toolbar-popover/toolbar-popover.page';
import { Uid } from '@ionic-native/uid/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers:[LoadingService,ScreenOrientation, BluetoothSerial,Storage, AndroidPermissions, Uid]
})
export class HomePage {

  public devices:any[] = []
  public statusBluetoothConection:boolean=false
  public flagBegin:boolean=false
  public toogleLampada=false
  public flagLedAceso=false
  public range=100
  public mac=''
  private flagBluetoothEnabled=false;
  private _storage: Storage | null = null;
  public contErrorBtnA=0
  public contErrorBtnB=0
  public contErrorBtnC=0

  public credentials = {
    senha:'',
    id:'',
    cpf:''
  }


  constructor(private loadingService:LoadingService, 
    private ngZone:NgZone,
    private androidPermissions: AndroidPermissions,
    private storage: Storage,
    private alertController: AlertController,
    private screenOrientation: ScreenOrientation,
    private popoverController: PopoverController,
    private uid: Uid,
    private bluetoothSerial: BluetoothSerial) 
  {
    // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BLUETOOTH).then(
    //   result => {
    //     if(result)
    //     {
    //       this.bluetoothSerial.enable()
    //       this.flagBluetoothEnabled=true;
    //     }
    //   },
    //   err => {
    //     this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BLUETOOTH)
    //   }
    // );
    
    // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.BLUETOOTH, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT)

    //this.bluetoothSerial.enable()

    // document.addEventListener('deviceready', onDeviceReady, false);
    // function onDeviceReady() {
    //    this.loadingService.presentToast(""+this.uid.IMEI);
    //}

    this.getPermission()

    //this.flagBluetoothEnabled=true;
  }

  // doRefresh(event) {
  //   console.log('Begin async operation');

  //   setTimeout(() => {
  //     console.log('Async operation has ended');
  //     this.toogleLampada=true;
  //     event.target.complete();
  //   }, 2000);
  // }

  public  getPermission(){
   
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BLUETOOTH).then(
      result => {
        if(result)
        {
          this.bluetoothSerial.enable().then((result)=>{
            if(result){
              this.initializerGateControl()
              this.flagBluetoothEnabled=true;
            }
          })
          .catch((err)=>{
            this.getPermission()
          })
        }
      },
      err => {
        this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BLUETOOTH)
      }
    );
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.BLUETOOTH, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
  }

  public async initializerGateControl()
  {
     // if(this.flagBluetoothEnabled)
    // {
    //   this.refresh()
    await this.storage.create();
    // this.storage.set("MAC","")
    // this.storage.set("cadastrado","false")
    // //this.storage.set("cadastrado","false")
    this.storage.get('cadastrado').then((val) => {
      if(val!="true")
      {
        this.presentConfPopover("$event")
      }else{
        this.storage.get('MAC').then((val) => {
          this.mac = val;
          //this.loadingService.present(this.mac)
          this.loadingService.present("Conectando, aproxime-se do portão ...")
          //this.bluetoothSerial.connect("30:AE:A4:0B:06:D6").subscribe(data=>{
          this.bluetoothSerial.connect(this.mac.toString()).subscribe(data=>{
            this.flagBegin=true;
            //this.loadingService.dismiss();
            this.loadingService.dismiss();
            this.loadingService.presentToast('Conectado ao dispositivo')
            this.statusBluetoothConection=true
            this.checkConnection()
            this.refresh()
          },error=>{
            this.flagBegin=true;  
            this.loadingService.dismiss();
            this.loadingService.presentToast("Erro na conexão " + error)
            this.loadingService.dismiss()
            this.statusBluetoothConection=false
            this.refresh()
            location.reload()
          })
        })
      }
    });
  }

  async ngOnInit() {

    // // if(this.flagBluetoothEnabled)
    // // {
    // //   this.refresh()
    //   await this.storage.create();
    //   // this.storage.set("MAC","")
    //   // this.storage.set("cadastrado","false")
    //   // //this.storage.set("cadastrado","false")
    //   this.storage.get('cadastrado').then((val) => {
    //     if(val!="true")
    //     {
    //       this.presentConfPopover("$event")
    //     }else{
    //       this.storage.get('MAC').then((val) => {
    //         this.mac = val;
    //         //this.loadingService.present(this.mac)
    //         this.loadingService.present("Conectando, aproxime-se do portão ...")
    //         //this.bluetoothSerial.connect("30:AE:A4:0B:06:D6").subscribe(data=>{
    //         this.bluetoothSerial.connect(this.mac.toString()).subscribe(data=>{
    //           this.flagBegin=true;
    //           //this.loadingService.dismiss();
    //           this.loadingService.dismiss();
    //           this.loadingService.presentToast('Conectado ao dispositivo')
    //           this.statusBluetoothConection=true
    //           this.checkConnection()
    //           this.refresh()
    //         },error=>{
    //           this.flagBegin=true;  
    //           this.loadingService.dismiss();
    //           this.loadingService.presentToast("Erro na conexão " + error)
    //           this.loadingService.dismiss()
    //           this.statusBluetoothConection=false
    //           this.refresh()
    //           location.reload()
    //         })
    //       })
    //     }
    //   });
    // }else{
    //   location.reload()
    // }
   
    //this.flagBegin=true;
    // // this.presentAlertConfirm()

    
    //this.printTeste()
  }

  public printTeste()
  {
    setTimeout( () => { 
      this.loadingService.presentToast(this. getID_UID("UUID")+"\n"+this. getID_UID("ICCID")+"\n"+this. getID_UID("IMSI")+"\n"+this. getID_UID("MAC")+"\n"+this. getID_UID(""))
      this.printTeste()
    }, 2000 );
  }

  public getID_UID(type){
    if(type == "IMEI"){
      return this.uid.IMEI;
    }else if(type == "ICCID"){
      return this.uid.ICCID;
    }else if(type == "IMSI"){
      return this.uid.IMSI;
    }else if(type == "MAC"){
      return this.uid.MAC;
    }else if(type == "UUID"){
      return this.uid.UUID;
    }
  }

 public awaitConfirm(btn:string)
 {
  setTimeout( () => { 
    this.bluetoothSerial.read()
    .then((data:string)=>{
      //let dataNomalized = data.toString().substring(1,data.length()-1)
      //let dataNomalized = data.toString().replace('0',"X")
      //alert(data.length)
      let message=''
      for(let i=0; i<data.length; i++)
      {
        if(this.isNumber(data[i]))
        {
          message+=data[i]
        }
      }

      if(message!="OK"){
        //alert(data)
        //this.awaitConfirm()
        this.loadingService.dismiss()
        if(btn == "A")
        {
          this.contErrorBtnA++
          if(this.contErrorBtnA>=2){
            this.presentAlertCommand(btn,message)
            this.loadingService.dismiss()
            this.contErrorBtnA=0
          }else{
            this.loadingService.present("Acionando o botão <strong> A novavente </strong>, aguarde... ")
            setTimeout( () => { 
              this.pressBtnA()
              this.loadingService.dismiss()
            },3000)
          }
        }

        else if(btn == "B")
        {
          this.contErrorBtnB++
          if(this.contErrorBtnB>=2){
            this.presentAlertCommand(btn,message)
            this.loadingService.dismiss()
            this.contErrorBtnB=0
          }else{
            this.loadingService.present("Acionando o botão <strong> B novavente </strong>, aguarde... ")
            setTimeout( () => { 
              this.loadingService.dismiss()
              this.pressBtnB()
            },3000)
          }
        }

        else if(btn == "C")
        {
          this.contErrorBtnC++
          if(this.contErrorBtnC>=2){
            this.presentAlertCommand(btn,message)
            this.loadingService.dismiss()
            this.contErrorBtnC=0
          }else{
            this.loadingService.present("Acionando o botão <strong> C novavente </strong>, aguarde... ")
            setTimeout( () => { 
              this.loadingService.dismiss()
              this.pressBtnC()
            },3000)
          }
        }
        
      }
      else{
        //alert("BATEU"+message)
        this.loadingService.dismiss()
        this.loadingService.presentToast("Acionamento efetuado com sucesso !!")
        this.contErrorBtnA=0
        this.contErrorBtnB=0
        this.contErrorBtnC=0
        this.refresh()
      }
    })
  }, 3000 );
 }

  public pressBtnA()
  {
    //this.loadingService.presentToast("BTN A")
    this.getCredentials()
    this.bluetoothSerial.isConnected().then(success => {
      this.bluetoothSerial.write('CmdBtnA'+JSON.stringify(this.credentials)+'CmdBtnA').then(success => {
      this.flagLedAceso=true
      this.refresh()
      setTimeout( () => { 
        this.flagLedAceso=false
        this.loadingService.present("Aguardando confirmação de acionamento !!!")
        this.awaitConfirm("A")
      }, 1000 );
      }, error => {
        this.loadingService.presentToast("Erro ao transmitir o comando ! - " + error)
        this.statusBluetoothConection=false
        location.reload();
      });
    }, error => {
      this.loadingService.presentToast("Dispositivo Desconectado " + error)
      this.statusBluetoothConection=false
      location.reload();
    });
  }

  public pressBtnB()
  {
    this.getCredentials()
    this.bluetoothSerial.isConnected().then(success => {
      this.bluetoothSerial.write('CmdBtnB'+JSON.stringify(this.credentials)+'CmdBtnB').then(success => {
        this.flagLedAceso=true
        this.refresh()
        setTimeout( () => { 
          this.flagLedAceso=false
          this.loadingService.present("Aguardando confirmação de acionamento !!!")
          this.awaitConfirm("B")
        }, 1000 );
      }, error => {
        this.loadingService.presentToast("Erro ao transmitir o comando ! - " + error)
        this.statusBluetoothConection=false
        location.reload();
      });
    }, error => {
      this.loadingService.presentToast("Dispositivo Desconectado " + error)
      this.statusBluetoothConection=false
      location.reload();
    });
  }

  public pressBtnC()
  {
    this.getCredentials()
    this.bluetoothSerial.isConnected().then(success => {
      this.bluetoothSerial.write('CmdBtnC'+JSON.stringify(this.credentials)+'CmdBtnC').then(success => {
        this.flagLedAceso=true
        this.refresh()
        setTimeout( () => { 
          this.flagLedAceso=false
          this.loadingService.present("Aguardando confirmação de acionamento !!!")
          this.awaitConfirm("C")
        }, 1000 );
      }, error => {
        this.loadingService.presentToast("Erro ao transmitir o comando ! - " + error)
        this.statusBluetoothConection=false
        location.reload();
      });
    }, error => {
      this.loadingService.presentToast("Dispositivo Desconectado " + error)
      this.statusBluetoothConection=false
      location.reload();
    });
  }

  // public pressBtnDelete()
  // {
  //   this.presentAlertConfirm();
  // }


  async presentConfPopover(ev:any){

    const popover = await this.popoverController.create({
      component: ConfPopoverPage,
      event: ev,
      translucent: true,
      keyboardClose:false,
      backdropDismiss:false
    });
    return await popover.present();

  }

  async presentToolbarPopover(ev:any){

    const popover = await this.popoverController.create({
      component: ToolbarPopoverPage,
      event: ev,
      translucent: true,
      keyboardClose:true,
      backdropDismiss:true
    });
    return await popover.present();
  }

  public getCredentials()
  {
    this.storage.get('SENHA').then((val) => {
      this.credentials.senha = val;
    })

    this.storage.get('ID').then((val) => {
      this.credentials.id = val;
    })

    this.storage.get('CPF').then((val) => {
      this.credentials.cpf = val;
    })

  }

  public checkConnection() {
    this.bluetoothSerial.isConnected().then(success => {
      this.statusBluetoothConection=true
    }, error => {
      this.loadingService.presentToast("Dispositivo Desconectado " + error)
      this.statusBluetoothConection=false
      location.reload();
    });
  }

  async presentAlertCommand(btn:string, message:string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Falha na confirmação !',
      message: 'Ocorreu uma <strong>falha na confirmação</strong> do acionamento do botão '+btn+', tente novamente !!',
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return;
          }
        }
      ]
    });

    await alert.present();
  }

  refresh() {
    this.ngZone.run(() => {
      console.log('force update the screen');
    });
  }

  public isNumber(data:any) : boolean
  { 
    if(data=='0' || data=='1' || data=='2' || data=='3' || data=='4' || data=='5' || data=='6' || data=='7' || data=='8' || data=='9')
    {
      return false
    }else{
      return true
    }
  }

  // public pressBtnPortao(){
    
  //   this.bluetoothSerial.isConnected().then(success => {
  //     this.bluetoothSerial.write('Cmd100').then(success => {
  //       this.loadingService.presentToast("Comando transmitido com sucesso !")
  //     }, error => {
  //       this.loadingService.presentToast("Erro ao transmitir o comando ! - " + error)
  //       this.statusBluetoothConection=false
  //       location.reload();
  //     });
  //   }, error => {
  //     this.loadingService.presentToast("Dispositivo Desconectado " + error)
  //     this.statusBluetoothConection=false
  //     location.reload();
  //   });
  // }

  // public pressBtnCampainha(){
  //   this.bluetoothSerial.isConnected().then(success => {
  //     this.bluetoothSerial.write('Cmd500').then(success => {
  //       this.loadingService.presentToast("Comando transmitido com sucesso !")
  //     }, error => {
  //       this.loadingService.presentToast("Erro ao transmitir o comando ! - " + error)
  //       this.statusBluetoothConection=false
  //       location.reload();
  //     });
  //   }, error => {
  //     this.loadingService.presentToast("Dispositivo Deconectado " + error)
  //     this.statusBluetoothConection=false
  //     location.reload();
  //   });
  // }

  // public pressToggle()
  // {
  //   if(this.toogleLampada){
  //     this.bluetoothSerial.isConnected().then(success => {
  //       this.bluetoothSerial.write('Liga2').then(success => {
  //         this.loadingService.presentToast("Comando transmitido com sucesso !")
  //       }, error => {
  //         this.loadingService.presentToast("Erro ao transmitir o comando ! - " + error)
  //         this.statusBluetoothConection=false
  //         location.reload();
  //       });
  //     }, error => {
  //       this.loadingService.presentToast("Dispositivo Deconectado " + error)
  //       this.statusBluetoothConection=false
  //       location.reload();
  //     });
  //   }else{
  //     this.bluetoothSerial.isConnected().then(success => {
  //       this.bluetoothSerial.write('Liga1').then(success => {
  //         this.loadingService.presentToast("Comando transmitido com sucesso !")
  //       }, error => {
  //         this.loadingService.presentToast("Erro ao transmitir o comando ! - " + error)
  //         this.statusBluetoothConection=false
  //         location.reload();
  //       });
  //     }, error => {
  //       this.loadingService.presentToast("Dispositivo Deconectado " + error)
  //       this.statusBluetoothConection=false
  //       location.reload();
  //     });
  //   }
  // }

  // public onSliderChanged(){
  //   this.bluetoothSerial.isConnected().then(success => {
  //     let comando = "DAC" + this.range + "AJ"
  //     //console.log(comando)
  //     this.bluetoothSerial.write(comando).then(success => {
  //       this.loadingService.presentToast("Comando transmitido com sucesso !")
  //     }, error => {
  //       this.loadingService.presentToast("Erro ao transmitir o comando ! - " + error)
  //       this.statusBluetoothConection=false
  //       location.reload();
  //     });
  //   }, error => {
  //     this.loadingService.presentToast("Dispositivo Deconectado " + error)
  //     this.statusBluetoothConection=false
  //     location.reload();
  //   });
  // }


  


  // public onDeviceDicovered(device){
  //   //console.log('Discovered' + JSON.stringify(device,null,2))
  //   this.ngZone.run(()=>{
  //     this.devices.push(device)
  //     console.log(device)
  //   })
  // }

 

}
