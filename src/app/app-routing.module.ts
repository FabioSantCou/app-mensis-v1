import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'conf-popover',
    loadChildren: () => import('./conf-popover/conf-popover.module').then( m => m.ConfPopoverPageModule)
  },
  {
    path: 'toolbar-popover',
    loadChildren: () => import('./toolbar-popover/toolbar-popover.module').then( m => m.ToolbarPopoverPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
